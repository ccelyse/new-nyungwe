<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccommodationGallery extends Model
{
    protected $table = "accomodationgallery";
    protected $fillable = ['id','room_name_id','room_picturename'];
}
