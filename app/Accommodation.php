<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Accommodation extends Model
{
    protected $table = "accomodation";
    protected $fillable = ['id','room_name','room_price','room_type','room_description','room_coverpic'];
}
