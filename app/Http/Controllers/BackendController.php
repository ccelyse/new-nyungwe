<?php

namespace App\Http\Controllers;

use App\Accommodation;
use App\AccommodationGallery;
use App\Http\Controllers\Controller;
use App\Attractions;
use App\User;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class BackendController extends Controller
{
    public function SignIn_(Request $request){

        if (Auth::attempt(['email' => $request['email'], 'password' => $request['password']])) {

            $user_get_role = User::where('email', $request['email'])->value('role');

            switch ($user_get_role) {
                case "Admin":
                    return redirect()->intended('/Accommodations');
                    break;
                case "Doctor":
                    return redirect()->intended('/DoctorDashboard');
                    break;
                case "Laboratory":
                    return redirect()->intended('/LaboratoryDashboard');
                    break;
                case "Accountant":
                    return redirect()->intended('/AccountantDashboard');
                    break;
                case "Reception":
                    return redirect()->intended('/ReceptionDashboard');
                    break;
                case "Nurse":
                    return redirect()->intended('/NurseDashboard');
                    break;
                case "Hospitalization":
                    return redirect()->intended('/HospitalizationDashboard');
                    break;
                default:
                    return back();
                    break;
            }

        }
    }
    public function getLogout(){
        Auth::logout();
        return redirect()->route('welcome');
    }

    public function AccountList(){
        $listaccount = User::all();
        return view('backend.AccountList')->with(['listaccount'=>$listaccount]);
    }
    public function Dashboard(){

        $insurancenumber = InsuranceData::select(DB::raw('count(id) as insurance'))->value('insurance');
        $departmentnumber = DepartmentData::select(DB::raw('count(id) as department'))->value('department');
        $medicinenumber = MedicineData::select(DB::raw('count(id) as medicinenumber'))->value('medicinenumber');
        $labonumber = LaboratoryTest::select(DB::raw('count(id) as labonumber'))->value('labonumber');
        $roomnumber = RoomLevel::select(DB::raw('count(id) as roomnumber'))->value('roomnumber');
        return view('backend.Dashboard')->with(['insurancenumber'=>$insurancenumber,'departmentnumber'=>$departmentnumber,'medicinenumber'=>$medicinenumber,'labonumber'=>$labonumber,'roomnumber'=>$roomnumber]);

    }
    public function Accommodations(){
        $listacco = Accommodation::all();
        return view('backend.Accommodations')->with(['listacco'=>$listacco]);
    }
    public function AddAccommodation(Request $request){
        $all = $request->all();
        $addacco = new Accommodation();
        $addacco->room_name = $request['room_name'];
        $addacco->room_price = $request['room_price'];
        $addacco->room_type = $request['room_type'];
        $addacco->room_description = $request['room_description'];

        $image = $request->file('room_coverpic');
        $addacco->room_coverpic = $image->getClientOriginalName();
        $imagename = $image->getClientOriginalName();
        $destinationPath = public_path('/cmsImages');
        $image->move($destinationPath, $imagename);
        $addacco->save();

        $lastid = $addacco->id;
        $room_picturename = $request['room_picturename'];

        foreach ($room_picturename as $images){
            $image = $request->file('room_picturename');
            $addaccogallery = new AccommodationGallery();
            $addaccogallery->room_name_id = $lastid;
            $addaccogallery->room_picturename = $images->getClientOriginalName();
            $imagename = $images->getClientOriginalName();
            $destinationPath = public_path('/cmsGallery');
            $images->move($destinationPath, $imagename);
            $addaccogallery->save();
        }
        return back()->with('success','You have successfully added a new accommodation');
//        dd($all);
    }
    public function EditAccommodation(Request $request){
        $id = $request['id'];
        $all = $request->all();
//        dd($all);

        if($request->file('room_coverpic')){

            $editroom = Accommodation::find($id);
            $editroom->room_name = $request['room_name'];
            $editroom->room_price = $request['room_price'];
            $editroom->room_type = $request['room_type'];
            $editroom->room_description = $request['room_description'];

            $image = $request->file('room_coverpic');
            $editroom->room_coverpic = $image->getClientOriginalName();
            $imagename = $image->getClientOriginalName();
            $destinationPath = public_path('/cmsImages');
            $image->move($destinationPath, $imagename);
            $editroom->save();
            return back()->with('success','You have successfully edited accommodation');

        }else{
            dd(' ooopps');
            $editroom = Accommodation::find($id);
            $editroom->room_name = $request['room_name'];
            $editroom->room_price = $request['room_price'];
            $editroom->room_type = $request['room_type'];
            $editroom->room_description = $request['room_description'];
            $editroom->save();
            return back()->with('success','You have successfully edited accommodation');
        }


    }
    public function DeleteAccommodation(Request $request){
        $id = $request['id'];
        $delete = Accommodation::find($id);
        $delete->delete();
        $deletepic = AccommodationGallery::where('room_name_id',$id)->delete();

        return back()->with('success','You have successfully deleted accommodation');
    }

}
