<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ListMemberResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'typeofmembership' => $this->typeofmembership,
            'chamber' => $this->chamber,
            'companyname' => $this->companyname,
            'companycode' => $this->companycode,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
//        return parent::toArray($request);
    }
}
