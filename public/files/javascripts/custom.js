$(document).ready(function(){
	
	var home = $('body').hasClass('home');
	var wh   = $(window).height();

	if( home ){
		$('.slider-height').css({'height' : wh});
	} else {
		$('.slider-height').css({'height' : wh * 0.75});
	}

	// banner Slide
	$('#slides').superslides({
		hashchange: false,
        play: 7000,
        pagination: false,
        animation : 'fade',
		inherit_height_from:".slider-height",
	});
	
	// banner bg 
	$('.superslides .slide-item').each( function(){  
		var imgSrc = $(this).find('.hero-image').attr('src');
		$(this).css({'background-image': 'url(' + imgSrc + ')'});
	});

	new WOW().init();
	
	//galleryslides
	$("#galleryslides .owl-prev").text();
	$("#galleryslides").owlCarousel({
		items : 4, //10 items above 1000px browser width
		itemsDesktop : [1000,3], //5 items between 1000px and 901px
		itemsDesktopSmall : [990,3], // betweem 900px and 601px
		itemsTablet: [600,1], //2 items between 600 and 0
		itemsMobile : false,
		autoPlay: true,
		navigation : true, // itemsMobile disabled - inherit from itemsTablet option
		pagination: false
	});
	
	$("#galleryslides1").owlCarousel({
		items : 3, //10 items above 1000px browser width
		itemsDesktop : [1000,3], //5 items between 1000px and 901px
		itemsDesktopSmall : [990,2], // betweem 900px and 601px
		itemsTablet: [767,1],
		itemsMobiles: [479,1], //2 items between 600 and 0
		itemsMobile : false,
		autoPlay: true,
		pagination: true,
		navigation : false // itemsMobile disabled - inherit from itemsTablet option
	});	

	$('.gallery #galleryslides').magnificPopup({
	  delegate: 'a',
	  type: 'image',
	  tLoading: 'Loading image #%curr%...',
	  mainClass: 'mfp-img-mobile',
	  gallery: {
		enabled: true,
		navigateByImgClick: true,
		preload: [0,1] // Will preload 0 - before current, and 1 after the current image
	  },
	  image: {
		tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
		titleSrc: ''
	  }
	});	

	$('.gallery-page').magnificPopup({
	  delegate: 'a',
	  type: 'image',
	  tLoading: 'Loading image #%curr%...',
	  mainClass: 'mfp-img-mobile',
	  gallery: {
		enabled: true,
		navigateByImgClick: true,
		preload: [0,1] // Will preload 0 - before current, and 1 after the current image
	  },
	  image: {
		tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
		titleSrc: ''
	  }
	});	
	
	

	// equalheight
	equalheight('.eqImg');
	
	// room slide
	$(".room-nav").owlCarousel({
		items : 5, //10 items above 1000px browser width
		itemsDesktop : [992,5], //5 items between 1000px and 901px
		itemsDesktopSmall : [991,4], // betweem 900px and 601px
		itemsTablet: [767,2], //2 items between 600 and 0
		itemsMobile : false,
		autoPlay: true,
		navigation : true, // itemsMobile disabled - inherit from itemsTablet option
		pagination: false,
		margin:10
	});	
	
	// rc-slider
	$(".rc-slider").owlCarousel({
		items:1,
		itemsTablet: [1500,1],
		navigation : true, 
		pagination: false,
		autoplay : true,
		autoplaySpeed : 700,
		autoplayTimeout:4200,
	});
	
	// torquatos-slider
	$(".torquatos-slider").owlCarousel({
		items:1,
		itemsTablet: [1500,1],
		navigation : false, 
		pagination: true,
		autoplay : true,
		autoplaySpeed : 700,
		autoplayTimeout:4200,
	});
	
	//  phone toggle
	//$('.mb-ph').click(function(){
	//	$(this).find('.ph-hd').toggle();
	//});
	
	// footer mob nav
	$('.footer-menu-t').click(function(){
		$(this).siblings('.footer-menu').toggle();
	});
	
	// book mobile
	$('.book-mb').click(function(){
		$('.mobile-booking').animate({'top':'0', 'opacity':'1', 'zIndex':'999'});
		$('body').addClass('oHdn');
	});
	
	// nav 
	$('.mb-nav-btn').click(function(){
		$('.mobile-nav').animate({'top':'0', 'opacity':'1', 'zIndex':'999'});
		$('body').addClass('oHdn');
	});
	$('.closeMenu').click(function(){
		$(this).parent().animate({'top':'-200%', 'opacity':'0', 'zIndex':'-999'});
		$('body').removeClass('oHdn');
	});
	
	// readmore toggle
	$('#Next-block .more-content').click(function(){
		$(this).siblings('.big-featured-content').slideToggle();
		$(this).parents('.ft-sm').toggleClass('append');
		$('.more-content').find('i').toggleClass('fa-angle-down fa-angle-up', 400);
		$('.more-content span').html($('.more-content span').text() == 'Read More' ? 'Read Less' : 'Read More');
	})
	
	
	// Room page menu
	$('.hanberger-menu').click(function(e){
		$(this).toggleClass('open');
		e.preventDefault();
		$(this).next().slideToggle();
	})
	
	// Book Button // resize scroll
	var ww   = $(window).width();
	var wh   = $(window).height();
	var ws   = $(window).scrollTop();
	var home = $('body').hasClass('home')

	$('.searchbox-wrapper h2').click(function(){
		$('.header .searchbox-wrapper .inner-wrap').slideToggle();
		$('.searchbox-wrapper h2').find('i').toggleClass('fa-angle-up fa-angle-down', 400);
	});
	
	if( ws > (wh * 0.5)){
		$('.searchbox-wrapper .inner-wrap').hide();
		$('.header').addClass('fixed');
	}else if( home ){
		$('.searchbox-wrapper .inner-wrap').show();
		$('.header').removeClass('fixed');
	}else{
		$('.searchbox-wrapper .inner-wrap').show();
		$('.header').removeClass('fixed');
	}
	
	//$('.book-btn').click(function(){
	//	$('.pnl-msg').slideToggle();
	//	$(this).parents('.time-book').toggleClass('active');
	//});

	if($(".navigation-menu nav ul li").length == 0) {
		$(".navigation-menu").hide(); 
	};

	//Map Filters
	$('.location ul li').on('click', function(e) {
	    $(this).toggleClass('active').siblings().removeClass('active');
		e.preventDefault();
    });	
	
	
});

$(window).scroll(function(){
	// Book Button // resize scroll
	var ww   = $(window).width();
	var wh   = $(window).height();
	var ws   = $(window).scrollTop();
	var home = $('body').hasClass('home')
	
	if( ws > (wh * 0.5)){
		$('.header .searchbox-wrapper .inner-wrap').hide();
		$('.header').addClass('fixed');
	}else if( home ){
		$('.header .searchbox-wrapper .inner-wrap').show();
		$('.header').removeClass('fixed');
	}else{
		$('.header .searchbox-wrapper .inner-wrap').show();
		$('.header').removeClass('fixed');
	}
	
});

$(window).load(function(){
	equalheight('.eqImg');
	
	// Book Button // resize scroll
	var ww   = $(window).width();
	var wh   = $(window).height();
	var ws   = $(window).scrollTop();
	var home = $('body').hasClass('home')
	
	if( ws > (wh * 0.5)){
		$('.header .book-box .time-book').hide();
		$('.header').addClass('fixed');
	}else if( home ){
		$('.header .book-box .time-book').show();
		$('.header').removeClass('fixed');
	}else{
		$('.header .book-box .time-book').show();
		$('.header').removeClass('fixed');
	}

    $('body').fadeTo( "slow", 1 );
	
});

$(window).resize(function(){
	equalheight('.eqImg');
	
	// Book Button // resize scroll
	var ww   = $(window).width();
	var wh   = $(window).height();
	var ws   = $(window).scrollTop();
	var home = $('body').hasClass('home')
	
	if( ws > (wh * 0.5)){
		$('.book-box .time-book').hide();
		$('.header').addClass('fixed');
	}else if( home ){
		$('.book-box .time-book').show();
		$('.header').removeClass('fixed');
	}else{
		$('.book-box .time-book').show();
		$('.header').removeClass('fixed');
	}
	
	
	// Rooms slider	
	var wh = $(window).height();
	$('.room-slider').css({'max-height' : wh});
	$('.slick-list ').css({'max-height' : wh});
	$('.room-slider .slide-item').each( function(){  
		var imgSrc = $(this).find('.hero-image').attr('src');
		$(this).css({'background-image': 'url(' + imgSrc + ')', 'height': wh});
	});
	if( home ){
		$('.slider-height').css({'height' : wh});
	} else {
		$('.slider-height').css({'height' : wh * 0.75});
	}
	
});

//Scroll
	$(function() {
		$('a[href*=#]:not([href=#])').click(function() {
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top
					}, 1000);
					return false;
				}
			}
		});
	});
	
// equalheight
equalheight = function(container){
var currentTallest = 0,
     currentRowStart = 0,
     rowDivs = new Array(),
     $el,
     topPosition = 0;
 $(container).each(function() {
   $el = $(this);
   $($el).height('auto')
   topPostion = $el.position().top;

   if (currentRowStart != topPostion) {
     for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
       rowDivs[currentDiv].height(currentTallest);
     }
     rowDivs.length = 0; // empty the array
     currentRowStart = topPostion;
     currentTallest = $el.height();
     rowDivs.push($el);
   } else {
     rowDivs.push($el);
     currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
  }
   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
     rowDivs[currentDiv].height(currentTallest);
   }
 });
}


