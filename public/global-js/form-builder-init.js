function regenerateTs(resultCallback)
{
    if (!document.$$formBuilderInit) {
        document.$$formBuilderInit = true;
        jQuery.ajax({
            type: 'GET',
            url: '/forms2/getToken',
            contentType: "text/html;charset=UTF-8",
            cache: false,
            success: function (token) {
                if (resultCallback) {
                    resultCallback(token);
                } else {
                    jQuery(".frm-bldr input[name*='data[Forms2][system][ts]']").val(token);
                }
                document.$$formBuilderInit = false;
            },
            error: function() {
                document.$$formBuilderInit = false;
            }
        });
    }
}


jQuery(document).ready(function() {
    jQuery.each(jQuery('.frm-bldr'), function(index, el) {
        jQuery(el).validate();
    });

    var checkboxIsChecked = true;

    jQuery('.frm-bldr input[type=submit]').removeAttr('disabled');

    jQuery('.frm-bldr input[type=submit]').click(function() {

        var checkboxIsRequired = false;
        var countRequiredCheckbox = 0;
        var ariaInvalidNotFalse = 0;

        countRequiredCheckbox = jQuery(this).parent().parent().parent().find('.required_checkbox').length;
        ariaInvalidNotFalse = jQuery(this).parent().parent().parent().find('li.checkbox :input[type=hidden]:not([aria-invalid="false"])').length;

        checkboxIsRequired =  countRequiredCheckbox > 0 ? true : false;

        if (jQuery(this).parents('.frm-bldr').valid()) {
            if(checkboxIsRequired){
                if(ariaInvalidNotFalse === 0 && checkboxIsChecked) {
                    jQuery(this).hide();
                    jQuery('.checkbox_error').remove();
                    jQuery(this).parent().append('<div class="submit-loading"></div>');
                    return true;
                }else{
                    jQuery('.checkbox_error').remove();
                    jQuery(this).parent().append('<div style="color:red;" class="checkbox_error">The checkbox field is required.</div>');
                    return false;
                }
            }else{
                jQuery(this).hide();
                jQuery('.checkbox_error').remove();
                jQuery(this).parent().append('<div class="submit-loading"></div>');
                return true;
            }
        }else{
            return false;
        }
    });

    regenerateTs(function(token){
        jQuery('.frm-bldr').append('<input type="hidden" name="data[Forms2][system][ts]" value="' + token + '" />');
    })

    setInterval("regenerateTs()", 300000);

    jQuery('li.checkbox :input[type=checkbox]').click(function(e) {
        if(jQuery(this).parent().parent().parent().find('input[type=checkbox]:checked').length > 0){
            checkboxIsChecked = true;
            jQuery(this).parent().parent().parent().find('.required_checkbox').valid();
        }else{
            checkboxIsChecked = false;
        }
    });

    jQuery('li.radio input[type=radio]').click(function() {
        jQuery(this).parent().parent().parent().find('.required_radio').valid();
    });

    jQuery('li.radio_yes_no input[type=radio]').click(function() {
        jQuery(this).parent().parent().parent().find('.required_radio').valid();
    });

    jQuery.each(jQuery('.datepicker'), function(index, datepicker) {
        var datepicker_options = {dateFormat: 'dd/mm/yy', showOn: 'focus'};

        if (jQuery(datepicker).parent().find('.validation').val() == 'future_date_current') {
            datepicker_options['minDate'] = new Date();
        }

        jQuery(datepicker).datepicker(datepicker_options);
    });
});
