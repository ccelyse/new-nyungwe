<style>
    .header-navbar .navbar-container ul.nav li i.ficon {
        font-size: 1.5rem;
        background: #fffefe;
        border-radius: 50%;
        padding: 5px;
        color: #615f5f;
        border: 1px solid;
    }
</style>
<nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-dark navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mobile-menu d-md-none mr-auto">
                    <a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#">
                        <i class="ft-menu font-large-1"></i>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="navbar-brand" href="{{url('/')}}">
                        <img class="brand-logo" alt="modern admin logo" src="images/VFALogo.png">
                        {{--<h3 class="brand-text">Modern Admin</h3>--}}
                    </a>
                </li>
                <li class="nav-item d-md-none">
                    <a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile"><i class="la la-ellipsis-v"></i></a>
                </li>
            </ul>
        </div>
        <div class="navbar-container">
            <div class="collapse navbar-collapse justify-content-end" id="navbar-mobile">
                <ul class="nav navbar-nav">
                    <li class="nav-item"><a class="nav-link mr-2 nav-link-label" href="{{url('/')}}">Home</a></li>
                    <li class="nav-item"><a class="nav-link mr-2 nav-link-label" href="">The Campaign</a></li>
                    <li class="nav-item"><a class="nav-link mr-2 nav-link-label" href="">Endorsements</a></li>
                    <li class="nav-item"><a class="nav-link mr-2 nav-link-label" href="">News</a></li>
                    <li class="nav-item"><a class="nav-link mr-2 nav-link-label" href="">Contact Us</a></li>
                    <li class="nav-item"><a class="nav-link mr-2 nav-link-label" href="{{url('login')}}">Login</a></li>
                    <li class="nav-item"><a class="nav-link mr-2 nav-link-label" href=""><i class="ficon ft-twitter"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</nav>