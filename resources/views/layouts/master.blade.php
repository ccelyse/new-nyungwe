<!DOCTYPE html>
<html>
<head>
    <title>@yield('title')</title>
    <!-- Meta Tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="format-decetion" content="telephone=no"/>
    <meta name="keywords" content="" />
    <meta name="description" content="" />

    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="stylesheet" href="files/css/searchbox.scss.css">
    <link href="files/css/box-searchbox-fix.css" rel="stylesheet" type="text/css"/>
    <link href="files/stylesheets/styles.css" type="text/css" rel="stylesheet" />

    <meta name="msvalidate.01" content="38F7A42322AA18E2A0A321D7CB0A70FF"/></head>
<body class="home" style="opacity:0;">
<header class="header wow fadeInDown" data-wow-delay="0.5s">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="logo">
                    <a href="{{url('/')}}"><img src="files/images/logo.jpg" alt="logo" /></a>
                    <!-- BEGIN OF SWING TAG -->


                    <style>.gift-tag-swing{
                            display: inline-block;
                            position:absolute;
                            top: 100px;
                            left: 50%;
                            margin-left: -86px;
                            z-index: -2;
                            background-color:transparent !important
                        }
                        header.fixed .gift-tag-swing{
                            opacity: 0;
                            visibility: hidden;
                        }

                        @media (max-width: 767px){
                            .gift-tag-swing{
                                display:none !important;
                            }

                        }
                        @media (min-width: 768px){
                            .gift-tag-swing{
                                top: 82px;
                                right: 50px;
                                left: auto;
                            }

                        }
                        @media (min-width: 992px){
                            .gift-tag-swing{
                                top: 74px;
                                left: 42px;
                                margin-left: 0;
                            }
                            header.fixed .gift-tag-swing{
                                top: 32px;
                                left: 42px;
                                margin-left: 0;
                            }
                        }
                        @media (min-width: 1200px){
                            .gift-tag-swing{
                                top:94px;
                                left: 4px;
                                margin-left:-28px;
                            }
                            header.fixed .gift-tag-swing{
                                top: 31px;
                                left: 10px;
                                margin-left: 0;
                            }
                        }
                        @-webkit-keyframes swingimage{
                            0%{-webkit-transform:rotate(10deg)}
                            50%{-webkit-transform:rotate(-5deg)}
                            100%{-webkit-transform:rotate(10deg)}
                        }
                        @keyframes swinging{
                            0%{transform:rotate(10deg)}
                            50%{transform:rotate(-5deg)}
                            100%{transform:rotate(10deg)}
                        }
                        .swingimage{
                            -webkit-transform-origin:50% 0;
                            transform-origin:50% 0;
                            -webkit-animation:swinging 3.5s ease-in-out forwards infinite;
                            animation:swinging 3.5s ease-in-out forwards infinite
                        }
                    </style>

                </div>
                <div class="headerTop hidden-sm hidden-xs">
                    <span class="phone"> Login</span>
                    <span class="phone"><a href="tel:+250787109335"><i class="zmdi zmdi-hc-fw zmdi-phone"></i> +250 787 109 335</a></span>

                    <div class="clearfix"></div>
                    <nav class="mainMenu">
                        <ul>
                            <li class="first"><a href="{{url('Accommodation')}}">Accommodations</a>
                            </li>
                            <li class="first"><a href="{{url('AboutUs')}}">About Us</a>
                            </li>
                            <li><a href="{{url('Facilities')}}">Facilities</a>
                            </li>
                            <li><a href="">Gallery</a></li>
                            <li><a href="{{url('ContactUs')}}">Contact</a>

                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="book-box hidden-sm hidden-xs">
                </div>
                <div class="mobile-element pull-right visible-sm visible-xs">
                    <div class="mb-nav-btn">
                        <img src="files/images/menu.png" alt="">
                    </div>
                    <a href="tel:+250787109335">
                        <div class="mb-ph">
                            <i class="zmdi zmdi-hc-fw zmdi-phone"></i> <br>
                            <span>CALL</span>
                            <div class="ph-hd"> +250 787 109 335</div>
                            {{--<div class="ph-hd"> Login</div>--}}
                        </div>
                    </a>
                    <div class="book-mb">
                        <div class="book-hd open-searchbox-02">
                            <span class="hidden-sm hidden-xs">Book Now</span>
                            <span class="hidden-md hidden-lg">Book Now</span>
                            <i class="fa fa-calendar-check-o"></i>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</header>
<nav class="mobile-nav">
    <img class="closeMenu" src="files/images/closeMenu.png" alt="">
    <div class="container">
        <ul>
            <li class="visible-sm visible-xs"><a href="{{url('/')}}"><img src="files/images/logo.jpg" alt="logo" /></a> </li>
        </ul>

        <ul>
            <li class="first"><a href="{{url('Accommodation')}}">Accommodations</a>
            </li>
            <li class="first"><a href="{{url('AboutUs')}}">About Us</a>
            </li>
            <li><a href="{{url('Facilities')}}">Facilities</a>
            </li>
            <li><a href="">Gallery</a></li>
            <li><a href="{{url('ContactUs')}}">Contact</a>

            </li>
        </ul>
    </div>
</nav>




<!--Header End-->

<!-- 7835 -->
@yield('content')