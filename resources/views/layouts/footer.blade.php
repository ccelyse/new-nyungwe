<footer class="full-width">
    <div class="footer-nav hidden-lg hidden-md text-center">
        <img src="files/images/menu.png" alt="" class="footer-menu-t">
        <nav class="full-width footer-menu display-none">

            <ul>

                <li><a href="">Reviews</a></li>
                <li><a href="">Sitemap</a></li>
                <li><a href="">Privacy Policy</a></li>
                <li><a href="" target="_blank">Brochure</a></li>
            </ul>
        </nav>
    </div>
    <div class="footerTop">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p class="copyright hidden-lg hidden-md">©2020 Nyungwe Top View Hotel</p>
                    <div class="row hidden-lg hidden-md">
                        <div class="footer-mb-ph col-sm-6 col-xs-12 text-right">Phone : <a href="tel:+250787109335">+250787109335</a></div>
                        <div class="footer-mb-em col-sm-6 col-xs-12 text-left">Email : <a href="reservations@nyungwehotel.com">reservations@nyungwehotel.com</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div><!--footerTop-->
    <div class="footerBottom">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="copyfix">
                        <p class="copyright hidden-sm hidden-xs">©2020 Nyungwe Top View Hote</p>
                        <ul class="hidden-sm hidden-xs">
                            <li>Phone : <a href="tel:250787109335">+250787109335</a></li>
                            <li>Email : <a href="mailto:reservations@nyungwehotel.com">reservations@nyungwehotel.com</a></li>
                        </ul>
                    </div>
                    <div class="logo2">

                        <!-- GOOGLE LANGUAGES -->
                        <div id="google_translate_element"></div>

                    </div>
                </div>
            </div>
        </div>
    </div><!--footerBottom-->

</footer>