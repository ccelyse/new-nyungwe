@extends('layouts.master')

@section('title', 'Nyungwe Hotel')

@section('content')


<div class="slider-height" style="background-color: #000;">
    <div id="slides" class="home-slider superslides full-width">
        <div class="slides-container">

            <div class="slide-item">
                <img class="hero-image" src="cmsImages/11.jpeg" alt="">
                <div class="container">
                    <div class="verticalCenter slide-content">
                        <div class="verticalInner">
                            <h2>
                                Our Facilities
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="slide-item">
                <img class="hero-image" src="cmsImages/8.jpeg" alt="">
                <div class="container">
                    <div class="verticalCenter slide-content">
                        <div class="verticalInner">
                            <h2>
                                Our Facilities
                            </h2>
                        </div>
                    </div>
                </div>
            </div>



        </div>
        <nav class="slides-navigation">
            <a href="index.html#" class="next"></a>
            <a href="index.html#" class="prev"></a>
        </nav>
        <a class="nextblock wow fadeInDown"  data-wow-delay="0.5s" href="index.html#Next-block">
            <div class="verticalCenter">
                <div class="verticalInner">
                    <span>Explore</span>
                    <i class="fa fa-angle-down"></i>
                </div>
            </div>
        </a>
    </div>
    <!--Slider End-->

</div>


<section class="featuers full-width">
    <div class="container">
        <div class="row">

            <div class="col-md-6">
                <!-- ITEM 1 -->
                <div class="ft-big ft-big-1 text-center marginB-30 wow fadeInLeft" style="background-image: url('cmsImages/11.jpeg');">
                    <div class="ft-big-inner">
                        <h3>Ground Shuttle within the country from or to Nyungwe National Park or other destination as requested by the Guest</h3>

                        {{--<a href="" target="_self" class="more-content">Read More</a>--}}
                    </div>
                </div>

                <!-- ITEM 2 -->
                <div class="ft-sm  wow fadeInDown">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="ft-sm-img ft-sm-img-2 eqImg" style="background-image: url('cmsImages/UOL-71.jpg');"></div>
                        </div>
                        <div class="col-sm-6">
                            <div class="ft-sm-text eqImg">
                                <h3>Conference Room</h3>
                                {{--<p>Relaxing evening after exploring the Nyungwe national park, canopy walk and forest walk. </p>--}}
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <div class="col-md-6">
                <!-- ITEM 3 -->
                <div class="ft-sm marginB-30 wow fadeInUp">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="ft-sm-img ft-sm-img-1 eqImg" style="background-image: url('cmsImages/13.jpeg');"></div>
                        </div>
                        <div class="col-sm-6">
                            <div class="ft-sm-text eqImg">
                                <h3>Front Desk Services</h3>
                        </div>
                    </div>
                </div>
                <div class="row">
                        <div class="col-sm-6">
                            <div class="ft-sm-img ft-sm-img-1 eqImg" style="background-image: url('cmsImages/carimg1.jpg');"></div>
                        </div>
                        <div class="col-sm-6">
                            <div class="ft-sm-text eqImg">
                                <h3>Ticketing and reservation  </h3>
                            <p>Ticketing and reservation for Nyungwe related attractions as well as other attractions in the country including gorilla permits, Lake Kivu circuit tour </p>
                            <!--
                                <div class="big-featured-content full-width">
                                  <p>Serving breakfast, lunch and dinner in a tranquil setting overlooking the Grand Canal </p>
                                </div>
                                -->
                                {{--<a href="" target="_self" class="more-content">Read More</a>              </div>--}}
                            </div>
                        </div>
                    </div>

                <!-- ITEM 4 -->
                <div class="ft-big ft-big-2 text-center wow fadeInRight" style="background-image: url('cmsImages/14.jpeg');">
                    <div class="ft-big-inner">
                        <h3>Restaurant and Bar</h3>
                        <!--
                                    -->
                        <a href="" target="_self" class="more-content">Read More</a>          </div>
                </div>




            </div>

        </div><!--row-->
    </div><!--container-->
</section>


<div class="mapBlock-wrap full-width">
    <h3 class="heading1">Our Location</h3>
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d342282.55687988753!2d29.106133639126696!3d-2.565095567535544!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x19c2e5687dabc83b%3A0x84af4fc4c78016a9!2sNyungwe+Forest+National+Park!5e0!3m2!1sen!2srw!4v1546804939775" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
<!--Map End-->

<style>@media (max-width: 767px) {
        .slide-item h2 {
            display: none;
        }
    }
</style>
@include('layouts.footer')



<!--[if IE]>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<![endif]-->
<!--[if !IE]><!-->
<!-- JQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<!--<![endif]-->
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>

<script type="text/javascript" src="files/javascripts/plugins/plugins.js"></script>

<script type="text/javascript" src="files/javascripts/jquery.superslides.min.js"></script>
<script type="text/javascript" src="files/javascripts/bootstrap.min.js"></script>
<script type="text/javascript" src="files/javascripts/owl.carousel.min.js"></script>

<script type="text/javascript" src="files/javascripts/wow.js"></script>

<script type="text/javascript" src="files/javascripts/jquery.heapbox.min.js"></script>

<script type="text/javascript" src="files/javascripts/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="files/javascripts/custom.js"></script>

<!--  -->


</body>
</html>


@endsection