@extends('layouts.master')

@section('title', 'Nyungwe Hotel')

@section('content')


<div class="slider-height" style="background-color: #000;">
    <div id="slides" class="home-slider superslides full-width">
        <div class="slides-container">

            <div class="slide-item">
                <img class="hero-image" src="cmsImages/11.jpeg" alt="">
                <div class="container">
                    <div class="verticalCenter slide-content">
                        <div class="verticalInner">
                            <h2>
                                Welcome to Nyungwe Hotel
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="slide-item">
                <img class="hero-image" src="cmsImages/8.jpeg" alt="">
                <div class="container">
                    <div class="verticalCenter slide-content">
                        <div class="verticalInner">
                            <h2>
                                Welcome to Nyungwe Hotel
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="slide-item">
                <img class="hero-image" src="cmsImages/6.jpeg" alt="">
                <div class="container">
                    <div class="verticalCenter slide-content">
                        <div class="verticalInner">
                            <h2>
                                Welcome to Nyungwe Hotel
                            </h2>
                        </div>
                    </div>
                </div>
            </div>


        </div>
        <nav class="slides-navigation">
            <a href="index.html#" class="next"></a>
            <a href="index.html#" class="prev"></a>
        </nav>
        <a class="nextblock wow fadeInDown"  data-wow-delay="0.5s" href="index.html#Next-block">
            <div class="verticalCenter">
                <div class="verticalInner">
                    <span>Explore</span>
                    <i class="fa fa-angle-down"></i>
                </div>
            </div>
        </a>
    </div>
    <!--Slider End-->

</div>



<section id="Next-block" class="callout full-width">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 col-xs-12">
                <h1>We've got what it takes to make your stay great!</h1>
                <p>The Nyungwe national park is a home of various primate which are already accustomed to human presence including chimpanzees, black and white colobus, mangabey, and unique plant species including orchids.</p>
                <p>It is also home for endangered bird species making it a suitable destination for bird lovers Water falls and a canopy walk way, various hike trails and interpretation centre are also some of the key tourist pulling attractions&nbsp;</p>
                {{--<p>Located in the heart of the leafy Ballsbridge suburb and in close proximity of Grafton Street, Aviva Stadium, RDS, Bord Gais Theatre &amp; Convention Center Dublin.&nbsp;</p>--}}

                <a class="read more-content" href="javascript:void(0)">
                    <span>Read More</span> <i class="fa fa-angle-down"></i></a>
            </div><!--col-->
        </div><!--row-->
    </div><!--container-->
</section>
<!--call out end-->

<!-- 831,1872 -->


<section class="featuers full-width">
    <div class="container">
        <div class="row">

            <h1 style="padding-bottom: 20px;">Why Nyungwe Top View Hill Hotel ?</h1>

            <div class="col-md-6">
                <!-- ITEM 1 -->
                <div class="ft-big ft-big-1 text-center marginB-30 wow fadeInLeft" style="background-image: url('cmsImages/10.jpeg');">
                    <div class="ft-big-inner">
                        <h3>A view of sunrise from the eastern wing</h3>

                        <a href="" target="_self" class="more-content">Read More</a>
                    </div>
                </div>

                <!-- ITEM 2 -->
                <div class="ft-sm hidden-sm hidden-xs wow fadeInDown">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="ft-sm-img ft-sm-img-2 eqImg" style="background-image: url('cmsImages/5.jpeg');"></div>
                        </div>
                        <div class="col-sm-6">
                            <div class="ft-sm-text eqImg">
                                <h3>Canopy walk and forest walk</h3>
                                <p>Relaxing evening after exploring the Nyungwe national park, canopy walk and forest walk. </p>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <div class="col-md-6">

                <!-- ITEM 3 -->
                <div class="ft-sm marginB-30 wow fadeInUp">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="ft-sm-img ft-sm-img-1 eqImg" style="background-image: url('cmsImages/4.jpeg');"></div>
                        </div>
                        <div class="col-sm-6">
                            <div class="ft-sm-text eqImg">
                                <h3>Camp fire</h3>
                                <p>A camp fire in room or in open after dinner, with cultural dances performed by local artists on special days </p>
                                <!--
                                <div class="big-featured-content full-width">
                                  <p>Serving breakfast, lunch and dinner in a tranquil setting overlooking the Grand Canal </p>
                                </div>
                                -->
                                <a href="" target="_self" class="more-content">Read More</a>              </div>
                        </div>
                    </div>
                </div>

                <!-- ITEM 4 -->
                <div class="ft-big ft-big-2 text-center wow fadeInRight" style="background-image: url('cmsImages/7.jpeg');">
                    <div class="ft-big-inner">
                        <h3>Chef’s creative meals always with a daily twist</h3>
                        <!--
                                    -->
                        <a href="" target="_self" class="more-content">Read More</a>          </div>
                </div>

                <!-- ITEM 2 -->
                {{--<div class="ft-sm hidden-md hidden-lg wow fadeInDown">--}}
                    {{--<div class="row">--}}
                        {{--<div class="col-sm-6">--}}
                            {{--<div class="ft-sm-img ft-sm-img-2 eqImg" style="background-image: url('cmsListings/12004/5880b2558589c.jpg');"></div>--}}
                        {{--</div>--}}
                        {{--<div class="col-sm-6">--}}
                            {{--<div class="ft-sm-text eqImg">--}}
                                {{--<h3>Fitness</h3>--}}
                                {{--<p>The Nyungwe hotel Fitness Suite is open daily between 6:30am - 9:00pm and is open for hotel residents (over 18s) only. The gym offers a mixture of cardio and resistance equipment. </p>--}}
                                {{--<!----}}
                                {{--<div class="big-featured-content full-width">--}}
                                  {{--<p>The Nyungwe hotel Fitness Suite is open daily between 6:30am - 9:00pm and is open for hotel residents (over 18s) only. The gym offers a mixture of cardio and resistance equipment. </p>--}}
                                {{--</div>--}}
                                {{---->--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}


            </div>

        </div><!--row-->
    </div><!--container-->
</section>
<!--featuers end-->




<!-- 7839 -->



<section class="gallery full-width">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="heading1">gallery</h3>
                <h6 class="more"><a href="gallery.html">view more</a></h6>
                <div class="gallerywrapper">
                    <div id="galleryslides">



                        <div class="item">
                            <figure>
                                <div class="reasonThumb">
                                    <div class="verticalCenter">
                                        <div class="verticalInner">
                                            <img src="cmsImages/11.jpeg" alt="thumb" />
                                            <div class="tbd"></div>
                                        </div><!--verticalInner-->
                                    </div><!--verticalCenter-->
                                    <div class="thumbdetail">
                                        <div class="verticalCenter">
                                            <div class="verticalInner">
                                                <a href="cmsImages/11.jpeg" title="Lock Four 2 AB"><span></span></a>
                                            </div><!--verticalInner-->
                                        </div><!--verticalCenter-->
                                    </div><!--thumbdetail-->
                                </div><!--reasonThumb-->
                            </figure>
                        </div><!--item-->
                        <div class="item">
                            <figure>
                                <div class="reasonThumb">
                                    <div class="verticalCenter">
                                        <div class="verticalInner">
                                            <img src="cmsImages/2.jpeg" alt="thumb" />
                                            <div class="tbd"></div>
                                        </div><!--verticalInner-->
                                    </div><!--verticalCenter-->
                                    <div class="thumbdetail">
                                        <div class="verticalCenter">
                                            <div class="verticalInner">
                                                <a href="cmsImages/2.jpeg" title="signature footer gallery"><span></span></a>
                                            </div><!--verticalInner-->
                                        </div><!--verticalCenter-->
                                    </div><!--thumbdetail-->
                                </div><!--reasonThumb-->
                            </figure>
                        </div><!--item-->
                        <div class="item">
                            <figure>
                                <div class="reasonThumb">
                                    <div class="verticalCenter">
                                        <div class="verticalInner">
                                            <img src="cmsImages/3.jpeg" alt="thumb" />
                                            <div class="tbd"></div>
                                        </div><!--verticalInner-->
                                    </div><!--verticalCenter-->
                                    <div class="thumbdetail">
                                        <div class="verticalCenter">
                                            <div class="verticalInner">
                                                <a href="cmsImages/3.jpeg" title="signature footer gallery"><span></span></a>
                                            </div><!--verticalInner-->
                                        </div><!--verticalCenter-->
                                    </div><!--thumbdetail-->
                                </div><!--reasonThumb-->
                            </figure>
                        </div><!--item-->
                        <div class="item">
                            <figure>
                                <div class="reasonThumb">
                                    <div class="verticalCenter">
                                        <div class="verticalInner">
                                            <img src="cmsImages/1.jpeg" alt="thumb" />
                                            <div class="tbd"></div>
                                        </div><!--verticalInner-->
                                    </div><!--verticalCenter-->
                                    <div class="thumbdetail">
                                        <div class="verticalCenter">
                                            <div class="verticalInner">
                                                <a href="cmsImages/1.jpeg" title="signature footer gallery"><span></span></a>
                                            </div><!--verticalInner-->
                                        </div><!--verticalCenter-->
                                    </div><!--thumbdetail-->
                                </div><!--reasonThumb-->
                            </figure>
                        </div><!--item-->


                    </div><!--col-->
                </div>
            </div><!--row-->
        </div><!--galleryslides-->
    </div><!--container-->
</section>
<!--Gallery End-->




<div class="mapBlock-wrap full-width">
    <h3 class="heading1">Our Location</h3>
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d342282.55687988753!2d29.106133639126696!3d-2.565095567535544!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x19c2e5687dabc83b%3A0x84af4fc4c78016a9!2sNyungwe+Forest+National+Park!5e0!3m2!1sen!2srw!4v1546804939775" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
<!--Map End-->

<style>@media (max-width: 767px) {
        .slide-item h2 {
            display: none;
        }
    }
</style>
@include('layouts.footer')



<!--[if IE]>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<![endif]-->
<!--[if !IE]><!-->
<!-- JQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<!--<![endif]-->
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>

<script type="text/javascript" src="files/javascripts/plugins/plugins.js"></script>

<script type="text/javascript" src="files/javascripts/jquery.superslides.min.js"></script>
<script type="text/javascript" src="files/javascripts/bootstrap.min.js"></script>
<script type="text/javascript" src="files/javascripts/owl.carousel.min.js"></script>

<script type="text/javascript" src="files/javascripts/wow.js"></script>

<script type="text/javascript" src="files/javascripts/jquery.heapbox.min.js"></script>
<script type="text/javascript" src="files/javascripts/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="files/javascripts/custom.js"></script>

<!--  -->


</body>
</html>


@endsection