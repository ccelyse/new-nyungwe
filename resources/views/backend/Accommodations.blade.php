@extends('backend.layout.master')

@section('title', 'Nyungwe Hotel')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <link rel="stylesheet" type="text/css" href="backend/app-assets/vendors/css/forms/selects/select2.min.css">
    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Basic form layout section start -->
                <section id="basic-form-layouts">
                    <div class="row match-height">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    @if (session('success'))
                                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                            {{ session('success') }}
                                        </div>
                                    @endif
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form class="form-horizontal form-simple" method="POST" action="{{ url('AddAccommodation') }}" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="form-body">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Room Name</label>
                                                            <input type="text" id="projectinput1" class="form-control" placeholder=""
                                                                   name="room_name">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Room Price</label>
                                                            <input type="text" id="projectinput1" class="form-control" placeholder=""
                                                                   name="room_price" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Room Type</label>
                                                            <select class="form-control" name="room_type">
                                                                <option value="Queen Double Room">Queen Double Room</option>
                                                                <option value="Twin Room">Twin Room</option>
                                                            </select>
                                                    </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Room Cover Picture</label>
                                                        <input type="file" id="projectinput1" class="form-control"
                                                                   name="room_coverpic" required>
                                                    </div>
                                                </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label for="projectinput1">Room Description</label>
                                                        <div class="form-group">
                                                            <textarea class="form-control" name="room_description" rows="10"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <script type="text/javascript">
                                                    $(document).ready(function()
                                                    {
                                                        $('.multi-field-wrapper').each(function() {
                                                            var $wrapper = $('.multi-fields', this);
                                                            $(".add-field", $(this)).click(function(e) {
                                                                $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
                                                            });
                                                            $('.multi-field .remove-field', $wrapper).click(function() {
                                                                if ($('.multi-field', $wrapper).length > 1)
                                                                    $(this).parent('.multi-field').remove();
                                                            });
                                                        });
                                                    });
                                                </script>
                                                <div class="multi-field-wrapper">
                                                    <div class="multi-fields">
                                                        <div class="row  multi-field">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label for="projectinput1">Room Gallery</label>
                                                                    <input type="file" id="projectinput1" class="form-control"
                                                                           name="room_picturename[]">
                                                                </div>
                                                            </div>
                                                            <button type="button" class="remove-field btn btn-dark round" style="line-height: inherit !important;"><i class="fas fa-times-circle"></i></button>
                                                            <button type="button" class="add-field btn btn-dark round" style="line-height: inherit !important;"><i class="fas fa-plus-circle"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            <div class="form-actions">
                                                <button type="submit" class="btn btn-login">
                                                    <i class="la la-check-square-o"></i> Save
                                                </button>
                                            </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </section>
                <section id="form-control-repeater">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">

                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <table class="table table-striped table-bordered zero-configuration table-responsive">
                                            <thead>
                                            <tr>
                                                <th>Room Name</th>
                                                <th>Room Price</th>
                                                <th>Room Type</th>
                                                <th>Room Cover Picture</th>
                                                <th>Room Description</th>
                                                <th>Room Gallery</th>
                                                <th>Date Created</th>
                                                <th>Edit</th>
                                                <th>Delete</th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($listacco as $data)
                                                <tr>
                                                    <td>{{$data->room_name}}</td>
                                                    <td>$ {{$data->room_price}} / night</td>
                                                    <td>{{$data->room_type}}</td>
                                                    <td><img src="cmsImages/{{$data->room_coverpic}}" style="width: 100%"></td>
                                                    <td>{{$data->room_description}}</td>
                                                    <td>
                                                        <button type="button" class="btn btn-icon btn-outline-primary btn-min-width mr-1 mb-1"
                                                                data-toggle="modal"
                                                                data-target="#editnomie{{$data->id}}">View Gallery
                                                        </button>
                                                        <!-- Modal -->
                                                        <div class="modal fade text-left" id="editnomie{{$data->id}}" tabindex="-1"
                                                             role="dialog" aria-labelledby="myModalLabel1"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h4 class="modal-title" id="myModalLabel1">Gallery</h4>
                                                                        <button type="button" class="close" data-dismiss="modal"
                                                                                aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                       <?php
                                                                            $id = $data->id;
                                                                            $getpics = \App\AccommodationGallery::where('room_name_id',$id)->get();

                                                                        ?>
                                                                        @foreach($getpics as $pics)
                                                                            <div class="row">
                                                                                <div class="col-lg-12">
                                                                                    <img src="cmsGallery/{{$pics->room_picturename}}" style="width:100% ">
                                                                                </div>
                                                                            </div>

                                                                        @endforeach

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>{{$data->created_at}}</td>
                                                    <td>

                                                        <button type="button" class="btn btn-icon btn-outline-primary btn-min-width mr-1 mb-1"
                                                                data-toggle="modal"
                                                                data-target="#editplace{{$data->id}}">Edit
                                                        </button>
                                                        <!-- Modal -->
                                                        <div class="modal fade text-left" id="editplace{{$data->id}}" tabindex="-1"
                                                             role="dialog" aria-labelledby="myModalLabel1"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h4 class="modal-title" id="myModalLabel1">Edit</h4>
                                                                        <button type="button" class="close" data-dismiss="modal"
                                                                                aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <form class="form-horizontal form-simple" method="POST"
                                                                              action="{{ url('EditAccommodation') }}"
                                                                              enctype="multipart/form-data">
                                                                            {{ csrf_field() }}
                                                                            <div class="row">
                                                                                <input type="text" name="id" value="{{$data->id}}" hidden>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-6">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Room Name</label>
                                                                                        <input type="text" id="projectinput1" class="form-control" value="{{$data->room_name}}"
                                                                                               name="room_name">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Room Price</label>
                                                                                        <input type="text" id="projectinput1" class="form-control" value="{{$data->room_price}}"
                                                                                               name="room_price" required>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Room Type</label>
                                                                                        <select class="form-control" name="room_type">
                                                                                            <option value="{{$data->room_type}}">{{$data->room_type}}</option>
                                                                                            <option value="Queen Double Room">Queen Double Room</option>
                                                                                            <option value="Twin Room">Twin Room</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Room Cover Picture</label>
                                                                                        <input type="file" id="projectinput1" class="form-control"
                                                                                               name="room_coverpic" required>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <label for="projectinput1">Room Description</label>
                                                                                    <div class="form-group">
                                                                                        <textarea class="form-control" name="room_description" rows="10">{{$data->room_description}}</textarea>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-lg-12">
                                                                                    <img src="cmsImages/{{$data->room_coverpic}}" style="width: 100%">
                                                                                </div>
                                                                                <div class="col-md-12">
                                                                                    <button type="submit" class="btn btn-icon btn-outline-primary">
                                                                                        <i class="la la-check-square-o"></i> Save
                                                                                    </button>
                                                                                </div>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </td>
                                                    <td><a href="{{ route('backend.DeleteAccommodation',['id'=> $data->id])}}" class="btn btn-icon btn-outline-primary">Delete</a></td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    {{--<script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>--}}
    {{--<script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>--}}
    {{--<script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>--}}
    {{--<script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>--}}
    {{--<script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"--}}
            {{--type="text/javascript"></script>--}}
    <script src="backend/app-assets/vendors/js/forms/select/select2.full.min.js" type="text/javascript"></script>

    <script src="backend/app-assets/js/scripts/forms/select/form-select2.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS-->
    <!-- ////////////////////////////////////////////////////////////////////////////-->

@endsection