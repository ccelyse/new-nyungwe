@extends('layouts.master')

@section('title', 'Nyungwe Hotel')

@section('content')


    <div class="slider-height" style="background-color: #000;">

        <div id="slides" class="home-slider superslides full-width">
            <div class="slides-container">

                <div class="slide-item">
                    <img class="hero-image" src="cmsImages/11.jpeg" alt="">
                    <div class="container">
                        <div class="verticalCenter slide-content">
                            <div class="verticalInner">
                                <h2>About Us</h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slide-item">
                    <img class="hero-image" src="cmsImages/8.jpeg" alt="">
                    <div class="container">
                        <div class="verticalCenter slide-content">
                            <div class="verticalInner">
                                <h2>About Us</h2>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <nav class="slides-navigation">
                <a href="rooms.html#" class="next"></a>
                <a href="rooms.html#" class="prev"></a>
            </nav>
            <a class="nextblock wow fadeInDown"  data-wow-delay="0.5s" href="rooms.html#Next-block">
                <div class="verticalCenter">
                    <div class="verticalInner">
                        <span>Scroll</span>
                        <i class="fa fa-angle-down"></i>
                    </div>
                </div>
            </a>
        </div>
        <!--Slider End-->

    </div>



    <div id="Next-block" class="room-content-w full-width">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-push-1 text-center wow fadeInUp">
                    <h1 class="heading2">Location</h1>
                </div>
                <div class="col-md-12 text-left text-center-sm  wow fadeInUp">
                    <p>Located in Buvungira Cell of Bushekeri Sector of Nyamasheke district of the Western Province of Rwanda, Nyungwe Top View Hill Hotel is owned by a Rwandese entrepreneur and conservationist.In operation since February 2011, the hotel is composed of two distinct compartments. The main area consists of a large, two-tiered rondavel (see photo below) with a phenomenally high roof. Although a rondavel is usually a traditionally built round house, this building offers a rather more modern take on the theme. The ground floor, where you find the reception, is furnished with a few modern angular sofas and walls adorned with traditional Rwandan murals.Circular wicker lights hang from the ceiling and dark tiled stairs lead you up to a mezzanine balcony. Whereas the basement cubicle hosts administration offices, the reception, kitchen, store and the laundry, it is the second level that hosts dining area and the bar both with tables and chairs set on a rather out-of-place black-tiled floor and a sweeping veranda with all-weather tables and chairs of steel and moulded materials. The views from here are stunning and on clear days you can even see Lake Kivu and the volcanoes of far North Rwanda and the Eastern of Democratic Republic of Congo. The bar on this level is entirely decorated from bottle tops, giving it a bright but simple look.</p>

                    <p>As the names suggests Nyungwe Top View Hill Hotel is built on the top of one of the hills positioning it to have the most beautiful views in the Nyungwe Forest area. Located five minutes' drive off a main road, uphill, the hotel has 12 cottages arranged into two rows, one at the sunrise and the other at the sunset side. On each side, there is a row of six red-brick cottages ranged northward. Each of these double or twin cottages houses a lounge area at the front, with terracotta-tiled floors, sofa, chair and coffee table, and a fireplace which can be lit for you on cold evenings. The bedrooms are quite sparse, furnished with a bed, hanging space for clothes, hair dressing table and amazing well decorated spacious and clean bathroom.</p>

                </div>

                <div class="col-md-10 col-md-push-1 text-center wow fadeInUp">
                    <h1 class="heading2">Attraction</h1>
                </div>
                <div class="col-md-12 text-left text-center-sm  wow fadeInUp">
                    <p>The hotel is located on the vicinity of Nyungwe National Park which
                        harbours a number of tourism attractions including various primates that
                        are already accustomed to human presence including chimpanzees, black
                        and white colobus, mangabey, and unique plant species including orchids
                        making it special for visitors. Also the park is well located as it is situated
                        at the Congo Nile water Divide. Thus attractions including a trial denoted
                        the Congo-Nile has been created to enable nature ventures to experience
                        this wonder.</p>

                    <p>The forest is also home for endangered bird species making it a suitable
                        destination for birders. Waterfalls and a newly constructed canopy walk
                        way and interpretation centre are also some of the key tourists pulling
                        factors.</p>

                    <p>Furthermore, the hotel is well positioned to accommodate those willing to
                        visit the low land Gorilla of Kahuzi Biega National Park in DRC.</p>
                </div>

                <div class="col-md-10 col-md-push-1 text-center wow fadeInUp">
                    <h1 class="heading2">Accessibility</h1>
                </div>
                <div class="col-md-12 text-left text-center-sm  wow fadeInUp">
                    <p>The hotel is located at less than two kilometres from the main high way
                        road from Kigali to Bukavu. This makes its accessibility easier to travellers.</p>

                    <p>Additionally, the hotel is located at 30 minutes drive from the shore of
                        Lake Kivu. Being an attraction itself, Lake Kivu has been identified as an
                        important medium for transport of tourists who not only want to enjoy
                        scenic view of the lake but also who want to link mountain gorilla visit to
                        Nyungwe National Park and its surrounding areas. For this purpose there
                        is a boat to facilitate the Lake Kivu circuit which connects Rusizi to Rubavu
                        via Nyamasheke and Karongi thrice a week.</p>

                    <p>The site can also be accessible by air as it is located at 45 minutes drive
                        from Kamembe airport where a daily schedule is covered by Rwanda Air.</p>
                    <p>The hotel is also suitably located to accommodate people willing to do
                        business in Cyangugu and neighboring countries including the Democratic
                        Republic of Congo and Burundi. It is located at 45 minutes drive from
                        Bukavu (DRC) and 3 hours to Bujumbura (Burundi).</p>
                </div>
                <div class="col-md-10 col-md-push-1 text-center wow fadeInUp">
                    <h1 class="heading2">Uniqueness</h1>
                </div>
                <div class="col-md-12 text-left text-center-sm  wow fadeInUp">
                    <p>The site has an incomparable location view. From East, the site enjoys the
                        view of different parts of Nyungwe National Park forest canopy and a suite
                        of gentle hills. Northwards, the site has the view of Lake Kivu and a chain
                        of extinct and active volcanoes including Karisimbi, Mikeno, Nyamuragira
                        and Nyiragongo. On West, the site has marvellous view of terraces from
                        surrounding communities, Lake Kivu and a chain of mountain of Kahuzi
                        Biega National Park. On South the site has the view of Nyungwe forest
                        canopy and gentle hills of the park, tea estate plantations and terraces
                        from neighbouring community gardens.</p>

                    <p>Cottages have been built to fit the local environment with a mixture of
                        Rwandese traditional art work and the Modern interior design and
                        furnishing rendering visitors to comfortably enjoying the beauty of the
                        area.</p>

                    <p>The design of the main building has been inspired by the Rwandese
                        traditional circular housing type rendering the balcony, restaurant and bar
                        suitable to enjoy 360o panoramic view of the area.</p>

                    <p>Similarly every cottage has been constructed with a particular view which
                        the visitor enjoys while sited at the balcony. Thus every cottage has been
                        named after a particular corresponding view. For instance when one
                        wants to enjoy the view and breeze from Lake Kivu, the cottage named
                        Kivu will be the choice whereas cottage named Nyungwe enables visitor to
                        enjoy the view of Nyungwe forest.</p>
                    <p>Also this site enables visitors to enjoy both the sunrise and the sunset. For
                        those who enjoy sunrise the most, the Sunrise wing cottages would be
                        their choice whereas those who enjoy the sunset, the choice would be the
                        sunset wing cottages. The balcony in the main building also offers an
                        opportunity to enjoy either the sunrise or sunset.</p>
                </div>
                <div class="col-md-10 col-md-push-1 text-center wow fadeInUp">
                    <h1 class="heading2">Structure and Designs</h1>
                </div>
                <div class="col-md-12 text-left text-center-sm  wow fadeInUp">
                    <p>The circular structure of the main building, the use of local material of the
                        cottages and a well mixture of the high quality fittings and furniture with
                        traditional settings is not only an attraction but also an added value that
                        visitors admire</p>
                </div>

                <div class="col-md-12">
                    <div class="rc-slider full-width">

                            <div class="slide-rc full-width">
                                <img class="hero-image" src="cmsImages/2.jpeg" alt="">
                            </div>
                        <div class="slide-rc full-width">
                            <img class="hero-image" src="cmsImages/11.jpeg" alt="">
                        </div>
                        {{--<div class="slide-rc full-width">--}}
                            {{--<img class="hero-image" src="cmsImages/nyungwe3.png" alt="">--}}
                        {{--</div>--}}
                        {{--<div class="slide-rc full-width">--}}
                            {{--<img class="hero-image" src="cmsImages/nyungwe4.png" alt="">--}}
                        {{--</div>--}}

                    </div>
                </div>

            </div>
        </div>
    </div>

<style>@media (max-width: 767px) {
        .slide-item h2 {
            display: none;
        }
    }
</style>
@include('layouts.footer')



<!--[if IE]>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<![endif]-->
<!--[if !IE]><!-->
<!-- JQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<!--<![endif]-->
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>

<script type="text/javascript" src="files/javascripts/plugins/plugins.js"></script>

<script type="text/javascript" src="files/javascripts/jquery.superslides.min.js"></script>
<script type="text/javascript" src="files/javascripts/bootstrap.min.js"></script>
<script type="text/javascript" src="files/javascripts/owl.carousel.min.js"></script>

<script type="text/javascript" src="files/javascripts/wow.js"></script>

<script type="text/javascript" src="files/javascripts/jquery.heapbox.min.js"></script>
<script type="text/javascript" src="files/javascripts/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="files/javascripts/custom.js"></script>

<!--  -->


</body>
</html>


@endsection



