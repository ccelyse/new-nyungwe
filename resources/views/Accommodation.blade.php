@extends('layouts.master')

@section('title', 'Nyungwe Hotel')

@section('content')


    <div class="slider-height" style="background-color: #000;">

        <div id="slides" class="home-slider superslides full-width">
            <div class="slides-container">

                <div class="slide-item">
                    <img class="hero-image" src="cmsImages/2.jpeg" alt="">
                    <div class="container">
                        <div class="verticalCenter slide-content">
                            <div class="verticalInner">
                                <h2>Rooms</h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slide-item">
                    <img class="hero-image" src="cmsImages/12.jpeg" alt="">
                    <div class="container">
                        <div class="verticalCenter slide-content">
                            <div class="verticalInner">
                                <h2>Rooms</h2>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <nav class="slides-navigation">
                <a href="rooms.html#" class="next"></a>
                <a href="rooms.html#" class="prev"></a>
            </nav>
            <a class="nextblock wow fadeInDown"  data-wow-delay="0.5s" href="rooms.html#Next-block">
                <div class="verticalCenter">
                    <div class="verticalInner">
                        <span>Scroll</span>
                        <i class="fa fa-angle-down"></i>
                    </div>
                </div>
            </a>
        </div>
        <!--Slider End-->

    </div>



    <!-- 834,1882 -->

    <div class="room-thumb full-width">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-push-1">
                    <div class="full-width room-nav wow fadeInDown">
                        @foreach($listacc as $rooms)
                        <div class="slide-link">
                            <a href="{{ route('AccommodationsMore',['id'=> $rooms->id])}}">
                                <img class="hero-image" src="cmsImages/{{$rooms->room_coverpic}}" alt="">
                                <p>{{$rooms->room_name}} </p>
                            </a>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--slide End-->



    <div id="Next-block" class="room-content-w full-width">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-push-1 text-center wow fadeInUp">
                    <h1 class="heading2">Nyungwe Hotel Accommodation</h1>
                </div>
                <div class="col-md-5 col-md-offset-1 text-left text-center-sm  wow fadeInUp">
                    <div>The site has an incomparable location view. From East, the site enjoys the view of different parts of Nyungwe National Park forest canopy and a suite of gentle hills. Northwards, the site has the view of Lake Kivu and a chain of extinct and active volcanoes including Karisimbi, Mikeno, Nyamuragira and Nyiragongo. On West, the site has marvellous view of terraces from surrounding communities, Lake Kivu and a chain of mountain of Kahuzi Biega National Park. On South the site has the view of Nyungwe forest canopy and gentle hills of the park, tea estate plantations and terraces from neighbouring community gardens.</div>
                    <div>&nbsp;</div>
                </div>

                <div class="col-md-5 text-left text-center-sm wow fadeInUp">
                    <div>Cottages have been built to fit the local environment with a mixture of Rwandese traditional art work and the Modern interior design and furnishing rendering visitors to comfortably enjoying the beauty of the area.&nbsp;</div>
                    <div>&nbsp;</div>
                    <div>The design of the main building has been inspired by the Rwandese traditional circular housing type rendering the balcony, restaurant and bar suitable to enjoy 360o panoramic view of the area.</div>
                    <div>&nbsp;</div>
                    <div>&nbsp;To download our brochure please click&nbsp;<a href="NYUNGWE TOP VIEW HILL HOTEL FACTS SHEET.pdf" target="_blank">here</a>.</div>
                </div>


                <div class="col-md-10 col-md-push-1">
                    <div class="full-width gray-border wow fadeInUp">
                        <div class="full-width icon-room">
                            <h3>Room Features</h3>

                            <!-- 835,1888 -->

                            <div class="col-sm-12">

                                <div class="icon-pg-w">
                                    <img src="cmsListings/12028/570f5bba7f260.jpg" alt=""> <span>Complimentary Mineral Water</span>
                                </div>
                                <div class="icon-pg-w">
                                    <img src="cmsListings/12029/570f5bce72d0c.jpg" alt=""> <span>In-room safe</span>
                                </div>
                                <div class="icon-pg-w">
                                    <img src="cmsListings/12040/570f68a83948a.jpg" alt=""> <span>Fully non-smoking</span>
                                </div>

                                <div class="icon-pg-w">
                                    <img src="cmsListings/12031/570f5c00ba790.jpg" alt=""> <span>Work desk & chair</span>
                                </div>

                                <div class="icon-pg-w">
                                    <img src="cmsListings/12034/570f5c4b86227.jpg" alt=""> <span>Rain fall shower</span>
                                </div>

                            </div>



                            <div class="full-width text-center wow fadeInDown">
                                <a href="#" class="book-room">Book Now</a>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- 7841 -->
            </div>
        </div>
    </div>

<style>@media (max-width: 767px) {
        .slide-item h2 {
            display: none;
        }
    }
</style>
@include('layouts.footer')



<!--[if IE]>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<![endif]-->
<!--[if !IE]><!-->
<!-- JQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<!--<![endif]-->
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>

<script type="text/javascript" src="files/javascripts/plugins/plugins.js"></script>

<script type="text/javascript" src="files/javascripts/jquery.superslides.min.js"></script>
<script type="text/javascript" src="files/javascripts/bootstrap.min.js"></script>
<script type="text/javascript" src="files/javascripts/owl.carousel.min.js"></script>

<script type="text/javascript" src="files/javascripts/wow.js"></script>

<script type="text/javascript" src="files/javascripts/jquery.heapbox.min.js"></script>


<script type="text/javascript" src="files/javascripts/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="files/javascripts/custom.js"></script>

<!--  -->


</body>
</html>


@endsection



