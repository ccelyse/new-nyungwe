@extends('layouts.master')

@section('title', 'Nyungwe Hotel')

@section('content')


    <div class="slider-height" style="background-color: #000;">

        <div id="slides" class="home-slider superslides full-width">
            <div class="slides-container">

                <div class="slide-item">
                    <img class="hero-image" src="cmsImages/DJI_0031.jpg" alt="">
                    <div class="container">
                        <div class="verticalCenter slide-content">
                            <div class="verticalInner">
                                <h2>Rooms</h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slide-item">
                    <img class="hero-image" src="cmsImages/DJI_0031.jpg" alt="">
                    <div class="container">
                        <div class="verticalCenter slide-content">
                            <div class="verticalInner">
                                <h2>Rooms</h2>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <nav class="slides-navigation">
                <a href="rooms.html#" class="next"></a>
                <a href="rooms.html#" class="prev"></a>
            </nav>
            <a class="nextblock wow fadeInDown"  data-wow-delay="0.5s" href="rooms.html#Next-block">
                <div class="verticalCenter">
                    <div class="verticalInner">
                        <span>Scroll</span>
                        <i class="fa fa-angle-down"></i>
                    </div>
                </div>
            </a>
        </div>
        <!--Slider End-->

    </div>

    <div id="Next-block" class="room-content-w full-width">
        <div class="container">
            <div class="row">
                @foreach($getroominfo as $info)
                <div class="col-md-10 col-md-push-1 text-center wow fadeInUp">
                    <h1 class="heading2">{{$info->room_name}}</h1>
                    <p>{{$info->room_description}}</p>
                </div>
                @endforeach


                <div class="col-md-10 col-md-push-1">
                    <div class="full-width gray-border wow fadeInUp">
                        <div class="full-width icon-room">
                            <h3>Room Features</h3>

                            <!-- 835,1891 -->

                            <div class="col-sm-12">
                                <div class="icon-pg-w">
                                    <img src="cmsListings/12026/570f5b9001627.jpg" alt=""> <span>Complimentary 500mb WiFi</span>
                                </div>
                                <div class="icon-pg-w">
                                    <img src="cmsListings/31919/5aec2a124f903.jpg" alt=""> <span>Air Con</span>
                                </div>
                                <div class="icon-pg-w">
                                    <img src="cmsListings/12042/570f5be6e8dd5.jpg" alt=""> <span>Smart TV</span>
                                </div>
                                <div class="icon-pg-w">
                                    <img src="cmsListings/31920/5aec2a2207296.jpg" alt=""> <span>Apple TV</span>
                                </div>
                                <div class="icon-pg-w">
                                    <img src="cmsListings/12043/570f6461d928f.jpg" alt=""> <span>Espresso Machine</span>
                                </div>
                                <div class="icon-pg-w">
                                    <img src="cmsListings/12027/570f5bac46790.jpg" alt=""> <span>Mini fridge</span>
                                </div>
                                <div class="icon-pg-w">
                                    <img src="cmsListings/12028/570f5bba7f260.jpg" alt=""> <span>Complimentary Mineral Water</span>
                                </div>
                                <div class="icon-pg-w">
                                    <img src="cmsListings/12029/570f5bce72d0c.jpg" alt=""> <span>In-room safe</span>
                                </div>
                                <div class="icon-pg-w">
                                    <img src="cmsListings/12044/570f645528241.jpg" alt=""> <span>Bathrobe & slippers</span>
                                </div>
                                <div class="icon-pg-w">
                                    <img src="cmsListings/12040/570f68a83948a.jpg" alt=""> <span>Fully non-smoking</span>
                                </div>
                                <div class="icon-pg-w">
                                    <img src="cmsListings/12031/570f5c00ba790.jpg" alt=""> <span>Work desk & chair</span>
                                </div>
                                <div class="icon-pg-w">
                                    <img src="cmsListings/12032/570f5c23cef59.jpg" alt=""> <span>Hair Dryer</span>
                                </div>
                                <div class="icon-pg-w">
                                    <img src="cmsListings/12034/570f5c4b86227.jpg" alt=""> <span>Rain fall shower</span>
                                </div>
                                <div class="icon-pg-w">
                                    <img src="cmsListings/31914/5aec670a35663.jpg" alt=""> <span>Business Center</span>
                                </div>
                                <div class="icon-pg-w">
                                    <img src="cmsListings/31917/5aec234ab3dfe.jpg" alt=""> <span>Fitness Suite</span>
                                </div>
                                <div class="icon-pg-w">
                                    <img src="cmsListings/31922/5aec2a415dec9.jpg" alt=""> <span>Room Service</span>
                                </div>
                            </div>



                            <div class="full-width text-center wow fadeInDown">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="rc-slider full-width">

                        @foreach($getroomgallery as $gallery)
                            <div class="slide-rc full-width">
                                <img class="hero-image" src="cmsGallery/{{$gallery->room_picturename}}" alt="">
                            </div>
                        @endforeach

                    </div>
                </div>

            </div>
        </div>
    </div>


<style>@media (max-width: 767px) {
        .slide-item h2 {
            display: none;
        }
    }
</style>
@include('layouts.footer')



<!--[if IE]>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<![endif]-->
<!--[if !IE]><!-->
<!-- JQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<!--<![endif]-->
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>

<script type="text/javascript" src="files/javascripts/plugins/plugins.js"></script>

<script type="text/javascript" src="files/javascripts/jquery.superslides.min.js"></script>
<script type="text/javascript" src="files/javascripts/bootstrap.min.js"></script>
<script type="text/javascript" src="files/javascripts/owl.carousel.min.js"></script>

<script type="text/javascript" src="files/javascripts/wow.js"></script>

<script type="text/javascript" src="files/javascripts/jquery.heapbox.min.js"></script>

<script type="text/javascript" src="files/javascripts/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="files/javascripts/custom.js"></script>

<!--  -->


</body>
</html>


@endsection



